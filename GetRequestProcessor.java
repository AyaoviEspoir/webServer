import java.io.*;
// 
import java.nio.charset.StandardCharsets;
/**
 * A GetRequestProcessor contains the logic necessary for handling HTTP GET requests.
 * 
 * @author Stephan Jamieson
 * @version 16/02/2016
 */

public class GetRequestProcessor extends RequestProcessor {

    /**
     * Create a new GetRequestProcessor</br>
     * Calling <code>getRequestMethod()</code> on this object returns <code>HTTPMethodType.GET</code>.
     */
    public GetRequestProcessor() {
        super(HTTPMethodType.GET);
    }
    
    /**
     * Process a given HTTP GET Request message, returning the result in an HTTP Response message.</br>
     */
    public Response process(final Request request) throws Exception {
        
        assert(this.canProcess(request.getMethodType()));
        System.out.println("Inside GetRequestProcessor.process()");
        Response response = new Response(request.getHTTPVersion());
        
        String filename = request.getURI();

        // Append trailing "/" with "index.html"
        if (filename.endsWith("/"))
            filename += "index.html";

        // Remove leading / from filename
        while (filename.indexOf("/") == 0)
            filename=filename.substring(1);

        // Replace "/" with "\" in path for PC-based servers
        filename = filename.replace('/', File.separator.charAt(0));

        // Check for illegal characters to prevent access to superdirectories
        if (filename.indexOf("..")>=0 || filename.indexOf(':')>=0
        || filename.indexOf('|')>=0)
            throw new FileNotFoundException();

        // If a directory is requested and the trailing / is missing,
        // send the client an HTTP request to append it.  (This is
        // necessary for relative links to work correctly in the client).
        if (new File(filename).isDirectory()) 
        {
            filename = filename.replace('\\', '/');
            response.setStatus(HTTPStatus.MOVED_PERMANENTLY);
            response.setHeaderField("Location: /", filename +"/");
            // TODO.
            // out.print("HTTP/1.0 301 Moved Permanently\r\n"+
            // "Location: /"+filename+"/\r\n\r\n");
            // out.close();
            return response;
        }

        // Open the file (may throw FileNotFoundException)
        // InputStream f=new FileInputStream(filename);

        // Determine the MIME type and print HTTP header
        String mimeType="text/plain";
        if (filename.endsWith(".html") || filename.endsWith(".htm"))
            mimeType="text/html";
        else if (filename.endsWith(".jpg") || filename.endsWith(".jpeg"))
            mimeType="image/jpeg";
        else if (filename.endsWith(".gif"))
            mimeType="image/gif";
        else if (filename.endsWith(".class"))
            mimeType="application/octet-stream";
        response.setStatus(HTTPStatus.OK);
        response.setHeaderField("Content-type: ", mimeType);
        response.setBody(new FileInputStream(filename));
        
        System.out.println("Leaving GetRequestProcessor.process()");

        return response;
    }
}