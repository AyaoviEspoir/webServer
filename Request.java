import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.FileNotFoundException;

//
import java.util.StringTokenizer;
/**
* Represents an HTTP Request type of message.
* A Request message has a start line comprising HTTP method type, universal 
* resource identifier (URI), and HTTP version.</br>
* It may contain contain header fields, and may have a body.
* 
* 
* 
*/
public class Request extends Message {

    private HTTPMethodType method;
    private String uri;
    private String HTTP_version;
    private byte[] body;
    
    public Request() { super(); }
    
    /**
     * Create a Request message with a request-line composed of the given method type, URI and HTTP version.
     */
    public Request(final HTTPMethodType method, final String uri, final String HTTP_version) {
        super();
        this.method=method;
        this.uri=uri;
        this.HTTP_version=HTTP_version;
    }

    private static HTTPMethodType getHttpMethodType(String httpMethodType)
    {
        if (httpMethodType.equals("GET"))
            return HTTPMethodType.GET;
        else if (httpMethodType.equals("POST"))
            return HTTPMethodType.POST;
        else if (httpMethodType.equals("PUT"))
            return HTTPMethodType.PUT;
        return null;
    }
        
    /**
     * Determine whether this request has a message body.
     */
    public boolean hasMessageBody() { return body!=null; }
    
    /**
     * Obtain the message body.</br>
     * Requires that <code>this.hasMessageBody()</code>.
     */
    public byte[] getBody() { return body; }
    
    /**
     * Obtain the request method type.
     */
    public HTTPMethodType getMethodType() { return this.method; }
    
    /**
     * Obtain the requested URI.
     */
    public String getURI() { return this.uri; }
    
    /**
     * Obtain the message http version.
     */
    public String getHTTPVersion() { return this.HTTP_version; }
    
    /**
     * Obtain the message request line i.e. <code>this.getMethodType()+" "+this.getURI()+" "+this.getHTTPVersion()</code>
     */
    public String getStartLine() {   
        return this.getMethodType()+" "+this.getURI()+" "+this.getHTTPVersion(); 
    }
        
    /**
     * Read an HTTP request from the given input stream and return it as a Request object.
     */
    public static Request parse(final InputStream input) throws IOException {
        // Code here.
        Request request = null;
        BufferedReader in = new BufferedReader(new InputStreamReader(input));
        String s = in.readLine();
        // String s = "GET /fire_fist_ace.png HTTP/1.1";
        // System.out.println(s);
        // System.out.println(s);  // Log the request

        // Attempt to serve the file.  Catch FileNotFoundException and
        // return an HTTP error "404 Not Found".  Treat invalid requests
        // the same way.
        StringTokenizer st = new StringTokenizer(s);
        HTTPMethodType httpMethodType = null;
        String uri = null;
        String httpVersion = null;

        // Parse the filename from the GET command

        if (st.hasMoreElements())
            httpMethodType = getHttpMethodType(st.nextToken());
        // else
        //     throw new FileNotFoundException();  // Bad request

        if (httpMethodType != null && st.hasMoreElements())
            uri = st.nextToken();
        // else
        //     throw new FileNotFoundException();  // Bad request

        if (httpMethodType != null && st.hasMoreElements())
            httpVersion = st.nextToken();
        // else
        //     throw new FileNotFoundException();  // Bad request
        
        request = new Request(httpMethodType, uri, httpVersion);

        return request;
    }

    public String toString()
    {
        return method + " " + uri + " " + HTTP_version;
    } 
}