import java.net.ServerSocket;
import java.net.Socket;
import java.io.*;

/**
 * Simple Web Server.
 * 
 * The server opens a TCP port and listens on that port for HTTP requests.
 * The server accepts a port number as an optional parameter.</br>
 * If no parameter is given then it requests one be randomly assigned when
 * opening the TCP server socket.</br>
 * In all cases, the server prints out the port that it is using.
 * 
 * 
 * @author Stephan Jamieson
 * @version 16/02/2016
 */
public class WebServer 
{

    private WebServer() {}
    /**
     * Run the web server. The server accepts a port number as an optional parameter.</br>
     * If no parameter is given then it requests one be randomly assigned when opening the TCP server socket.</br>
     * In all cases, the server prints out the port that it is using.
     */
    public static void main(String argv[]) throws Exception 
    {
		// Get the port number from the command line.
		int portNumber = argv.length>0 ?(new Integer(argv[0])).intValue():2222;     // portNumber defaults to 2222 if none is given.

        ServerSocket serverSocket = null;
        // String line;
        // DataInputStream is;
        // PrintStream os;
        //Socket clientSocket = null;
        System.out.println("Webserver starting up on port 80");
        // System.out.println("(press ctrl-c to exit)");
        try { serverSocket = new ServerSocket(portNumber); }
        catch (IOException e) { System.out.println(e); }
        System.out.println("Waiting for connection");
        System.out.println("The server started, to quit is press <CTRL><C>");
        // Request request = Request.parse();
        // System.out.println(request);
        
        try 
        {
            // clientSocket = serverSocket.accept();
            // is = new DataInputStream(clientSocket.getInputStream());  
            // os = new PrintStream(clientSocket.getOutputStream());
            Socket clientSocket = serverSocket.accept();  // Wait for a client to connect
            new ClientHandler(clientSocket);  // Handle the client in a separate thread
            // while (true) 
            // {
            //     line = is.readLine();
            //     os.println("From server: "+line);
            // }
        }
        catch (IOException e) { System.out.println(e); }
    }
}

class ClientHandler extends Thread
{
    private Socket socket;

    public ClientHandler(Socket socket)
    {
        this.socket = socket;
        start();
    }

    public void run()
    {
        try
        {   
            // Open connections to the socket
            PrintStream out = new PrintStream(new BufferedOutputStream(
            socket.getOutputStream()));

            // Read filename from first input line "GET /filename.html ..."
            // or if not in this format, treat as a file not found.
            Request request = Request.parse(socket.getInputStream());
            GetRequestProcessor processor = new GetRequestProcessor();
            Response response = processor.process(request);
            Response.send(out, response);
        }
        catch(Exception e) { System.out.println(e); }
    }
}
